---
title: Romans series
date: 2021-04-29T13:28:41.172Z
published: true
tags:
  - name: tag
cover_image: ../../static/images/uploads/romans-series.png
description: Our Romans series from May - July at Stoneycroft
---
See in the picture for the dates, passages & speakers in our Romans series.